import java.util.*;

public class ArrayQueue<T> implements Queue<T>{

    public ArrayQueue() {
        this.pointer = -1;
        this.capacity = 2;
        this.queue = (T[]) new Object[capacity];
    }

    private ArrayQueue(T[] queue) {
        this.queue = queue;
    }

    private T[] queue;
    private int pointer;
    private int capacity;

    @Override
    public int size() {
        return pointer + 1;
    }

    @Override
    public boolean isEmpty() {
        return pointer == -1;
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i <= pointer; i++) {
            if (queue[i] == o) {
                return true;
            }
        }

        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {

            final ArrayQueue<T> q = new ArrayQueue<>(queue);

            @Override
            public boolean hasNext() {
                return !q.isEmpty();
            }

            @Override
            public T next() {
                return q.remove();
            }
        };
    }

    @Override
    public Object[] toArray() {
        return Arrays.copyOf(queue, pointer + 1);
    }

    @Override
    public Object[] toArray(Object[] a) {
        return Arrays.copyOf(queue, pointer + 1);
    }

    @Override
    public boolean add(Object o) {
        if ((pointer + 1) == capacity) {
            expand(capacity * 2);
        }
        pointer++;
        queue[pointer] = (T) o;
        return true;
    }

    private void expand(int newCapacity) {
        capacity = newCapacity;
        queue = Arrays.copyOf(queue, newCapacity);
    }

    @Override
    public boolean remove(Object o) {
        for (int i = 0; i < size(); i++) {
            if( queue[i] == o) {
                for (int j = i; j < size() - 1; j++) {
                    queue[j] = queue[j + 1];
                }
                pointer--;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        for (Object element:  c) {
            add(element);
        }
        return true;
    }

    @Override
    public void clear() {
        pointer = -1;
    }

    @Override
    public boolean retainAll(Collection c) {
        for (Object element : queue) {
            if (!c.contains(element)) {
                remove(c);
            }
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection c) {
        for (Object element : c) {
            if (contains(element)) {
                remove(element);
            }
        }
        return true;
    }

    @Override
    public boolean containsAll(Collection c) {
        for (Object element : c) {
            if (!contains(element)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean offer(Object o) {
        return add(o);
    }

    @Override
    public T remove() {
        if (!isEmpty()) {
            T o = queue[pointer];
            pointer--;
            return o;
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public T poll() {
        if (!isEmpty()) {
            T o = queue[pointer];
            pointer--;
            return o;
        } else {
            return null;
        }
    }

    @Override
    public T element() {
        if (!isEmpty()) {
            return queue[pointer];
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public T peek() {
        if (!isEmpty()) {
            return queue[pointer];
        } else {
            return null;
        }
    }
}
